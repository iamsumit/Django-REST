from .base import *

SECRET_KEY = env('DJANGO_SECRET_KEY', default='k0bei(nd4-5z3q#1zrz#w4vk!dlhbsidr1(v4&r*s@_+=vh6h%')

DEBUG = env.bool('DJANGO_DEBUG', default=True)